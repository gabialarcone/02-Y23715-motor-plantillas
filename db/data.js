//crar datos de los integrantes
exports.integrantes = [
    { nombre: "Gabriela",apellido: "Espinola",  matricula: "Y23715"},
    { nombre: "Zuanny",apellido: "Ortiz", matricula: "Y26230"},
    { nombre: "Nicolas",apellido: "Gimenez", matricula: "Y18433"},
    { nombre: "Diego",apellido: "Ramirez", matricula: "UG0095"},
    { nombre: "Yenifer",apellido: "Aguilera", matricula: "Y12954"},
    
]

exports.media = [
    {nombre: "Youtube", src:null, url:"https://www.youtube.com/embed/Rk7gnFCeVAY", matricula: "Y23715", titulo: "video favorito de youtube" },
    {nombre: "imagen", url:null, src:"/images/Bob Esponja.jpeg", matricula: "Y23715", titulo:'Imagen que me representa' },
    {nombre: "dibujo",url:null, src:"/images/bananamichi.png", matricula: "Y23715", titulo:'Mi dibujo'  },

    {nombre: "Youtube", src:null, url:"https://www.youtube.com/embed/210R0ozmLwg?si=bbjXLbzQS8fBaoTz", matricula: "UG0045",titulo: "video favorito de youtube" },
    {nombre: "imagen", url:null, src:"/images/imagen1Diego.jpeg", matricula: "UG0045",  titulo:'Imagen que me representa' },
    {nombre: "dibujo",url:null, src:"/images/imagen2Diego.jpeg", matricula: "UG0045", nombre: "dibujo" },

    {nombre: "Youtube", src:null, url:"https://www.youtube.com/watch?v=DjeGUE9ya5s&t=737s", matricula: "Y12954", titulo: "video favorito de youtube" },
    {nombre: "imagen", url:null, src:"imagen que no subio", matricula: "Y12954", titulo:'Imagen que me representa' },
    {nombre: "dibujo",url:null, src:"imagen que no subio", matricula: "Y12954", titulo:'Mi dibujo'  },

    {nombre: "Youtube", src:null, url:"https://www.youtube.com/embed/YZ8j6iO0ulw?si=3qmb7GQ0CxHN2C3Q", matricula: "Y18433",titulo: "video favorito de youtube" },
    {nombre: "imagen", url:null, src:"/images/mis_sueños.jpeg", matricula: "Y18433",  titulo:'Imagen que me representa'},
    {nombre: "dibujo",url:null, src:"/images/DibujoNico.png", matricula: "Y18433", titulo:'Mi dibujo'  },

    {nombre: "Youtube", src:null, url:"https://www.youtube.com/embed/Qes1RMK9a50?si=U5YTt8otRVQc1Ahk", matricula: "Y26230", titulo: "video favorito de youtube" },
    {nombre: "imagen", url:null, src:"/images/Extrovertida.jpg", matricula: "Y26230", titulo:'Imagen que me representa' },
    {nombre: "dibujo",url:null, src:"/images/Dibujo_Zuanny.png", matricula: "Y26230", titulo:'Mi dibujo' },


]


exports.tipomedia = [
    {
        nombre: "Youtube"

    },{
        nombre: "imagen"

    },{
        nombre: "dibujo"
    }
]

//exports.integrantes = integrantes;
//exports.media = media;


