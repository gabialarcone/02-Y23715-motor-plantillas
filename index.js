//importar express
const express = require("express");
const bd = require("./db/data")
const hbs = require("hbs");

//creamos la aplicacion express
const app = express();


app.use(express.static(__dirname + "/public"));
app.set('view engine', 'hbs')
app.set('views', __dirname + "/views");

hbs.registerPartials(__dirname + "/views/partials");  

app.get("/", (request, response) => {
     response.render("index", {
      integrantes: bd.integrantes,
       });
      });

      app.get("/curso/", (request, response) => {
        response.render("curso")
      })

  app.get("/word_cloud/", (request, response) => {
    response.render("word_cloud")
  })

  const matriculas = [...new Set(bd.media.map(item => item.matricula))];

app.get("/:matricula", (request, response) => {
    const matricula = request.params.matricula;
    // Se verifica si la matricula existe.
    if (matriculas.includes(matricula)) {
        const mediaFiltrada = bd.media.filter(item => item.matricula === matricula);
        const integrantesFiltrados = bd.integrantes.filter(item => item.matricula === matricula);
        response.render('integrantes', {
            tipoMedia: bd.tipoMedia,
            integrantes: integrantesFiltrados,
            media: mediaFiltrada,
        });
    }
});

  app.use((req, res, next) => {
  res.status(404).render('aviso'); // Renderizar el archivo mensaje.hbs desde la carpeta partials
});
    app.listen(3000, () => {
        console.log("Servidor corriendo en el puerto 3000");
        console.log("http://localhost:3000/");
    });


     